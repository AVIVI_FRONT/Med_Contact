function sliderInit() {
    var element = $('.js-slider-main');
    if (element.length) {
        element.slick({
            arrows: false,
            autoplay: true,
            autoplaySpeed: 3000
        });
        $('.js-back').on('click', function () {
            element.slick('slickPrev');
        } );
        $('.js-forward').on('click', function () {
            element.slick('slickNext');
        } );
    }
}

function articlesSliderInit() {
    var element = $('.js-articles');
    if (element.length) {
        element.slick({
            arrows: false
        });
        element.on('init', function () {
            var length = $('.articles__slide').length;
            $('.slider__count span').text(length);
        });
        element.on('afterChange', function () {
            var index = $('.slick-active').index();
            var currentSlide = element.slick('slickCurrentSlide');
            // articles__slide
            console.log(index);
            console.log(currentSlide);


            $('.slider__number span').text(currentSlide + 1);
        });
        $('.js-left').on('click', function () {
            element.slick('slickPrev');
        } );
        $('.js-right').on('click', function () {
            element.slick('slickNext');
        } );
    }
}

function showMenu() {
   var button = $('.js-menu-open');
    var list = $('.js-menu');
    $(document).on('click', '.js-menu-open', function () {
        if (list.hasClass('active')) {
            list.removeClass('active');
            button.removeClass('active');
        } else {
            list.addClass('active');
            button.addClass('active');
        }
        $(document).on('click touchstart', function () {
            if (!button.is(event.target) && button.has(event.target).length === 0 && !list.is(event.target) && list.has(event.target).length === 0)  {
                list.removeClass('active');
                button.removeClass('active');
            }
        })
    });
   
}

function popularAsks() {
        $(document).on('click', '.js-ask', function () {
            var btn = $(this);
            if (btn.hasClass('showed')) {
                btn.removeClass('showed');
                btn.parents('.more__item').find('.more__text').removeClass('showed');
            } else {
                btn.addClass('showed');
                btn.parents('.more__item').find('.more__text').addClass('showed');
            }
        });
}

function fancyboxImg() {
    $(".fancy-img").fancybox();
}

function formatState (state) {
    if (!state.id) {
        return state.text;
    }
    var baseUrl = "images/flags";
    var $state = $(
        '<span><img src="' + baseUrl + '/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
    );
    return $state;
};
function formatSt (state) {
    if (!state.id) {
        return state.text;
    }

    var baseUrl = "images/flags";
    var $state = $(
        '<span><img class="img-flag" /> <span></span></span>'
    );

    // Use .text() instead of HTML string concatenation to avoid script injection issues
    $state.find("span").text(state.text);
    $state.find("img").attr("src", baseUrl + "/" + state.element.value.toLowerCase() + ".png");

    return $state;
};

$(document).ready(function () {
    $('.js-go-select').select2({
        templateResult: formatState,
        templateSelection: formatSt
    });
    $('.js-go-select').on('select2:select', function (e) {
        $(this).parents('.phone-selector').addClass('added');
        var parent = $(this).parents('.phone-selector');
        var data = e.params.data;
        var data_id =  data.id;
        switch(data_id.toLowerCase()) {
            case "ua":
                parent.find('input').mask('+38 (999) 999-99-99 ');
                break;
            case "ru":
                parent.find('input').mask('+7 (999) 999-99-99 ');
                break;
            case "ge":
                parent.find('input').mask('+49 (999) 999-99-99 ');
                break;
            case "kz":
                parent.find('input').mask('+7 (999) 999-99-99 ');
                break;
            default:
                parent.find('input').unmask();
        }
    });
    showMenu();
    sliderInit();
    articlesSliderInit();
    popularAsks();
    fancyboxImg();
});